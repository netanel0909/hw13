#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include "Helper.h"



int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server server;

		server.serve(3366);
	}
	catch (std::exception& err)
	{
		std::cout << "Error: " << err.what() << std::endl; 
	}
	system("pause");
	return 0;
}

