#include "Client.h"
#include<string>
Client::Client()
{
	this->_clientSocket = 0;
	this->_cName = "";
}

Client::Client(std::string cName, SOCKET& socket)
{
	this->_cName = cName;
	this->_clientSocket = socket;
}

Client::~Client()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_clientSocket);
	}
	catch (...) {}
}

void Client::startConversation()
{
	char m[39];
	recv(_clientSocket, m, 38, 0);
	m[38] = 0;
	std::cout << "Message: " << m << std::endl;

	std::string s;
	std::cout << "Pls Enter name (4 bytes): " << std::endl;
	std::cin >> s;
	send(_clientSocket, s.c_str(), s.size(), 0);  
	std::cout << "Message sent to server: " << std::endl;


	recv(_clientSocket, m, 3, 0);
	m[3] = 0;
	std::cout << "Message from server: " << m << std::endl;
}

void Client::connect(std::string serverIP, int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;  
	sa.sin_addr.s_addr = inet_addr(serverIP.c_str());   

	int status = ::connect(_clientSocket, (struct sockaddr*)&sa, sizeof(sa));

	if (status == INVALID_SOCKET)
		throw std::exception("no connection!");
}
