﻿#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include "Helper.h"
#include <mutex>
#include <condition_variable>
#include <fstream>


mutex _lock1;
condition_variable _msgCon;


void Server::msgs()
{
	Helper h;
	string fCont = "";
	Client c;

	while (true)
	{
		try
		{
			unique_lock<mutex> Lock(_lock1);
			_msgCon.wait(Lock);
			while (!this->_msgs.empty())
			{
				message msg = this->_msgs.front();
				this->_msgs.pop();

				int code = msg._code;

				
				 if (code == 207) {
					u_file(msg._fCont);

					c = this->_clients.front();
					this->_clients.pop();
					this->_clients.push(c);

					sendAll(msg._fCont);
				}
				 else if (code == 200) {
					 this->_clients.push(Client(msg._un, msg._clientSocket));

					 fCont = read_file();

					 if (this->_clients.size() == 1)h.sendUpMessageToClient(msg._clientSocket, fCont, this->_clients.front()._cName, "", 1);

					 else
					 {
						 c = this->_clients.front();
						 this->_clients.pop();

						 h.sendUpMessageToClient(msg._clientSocket, fCont, c._cName, this->_clients.front()._cName, this->_clients.size() + 1);
						 p_clientF(c);
					 }
				 }
				else if (code == 204) {
					u_file(msg._fCont);
					sendAll(msg._fCont);
				}
				else {
					d_client(msg._clientSocket);
					closesocket(msg._clientSocket);

					fCont = read_file();

					if (this->_clients.size() != 0)
					{
						sendAll(fCont);
					}
				}
			}

			Lock.unlock();
		}
		catch (...)
		{
			cout << "Error";
		}


	}

}


Server::Server()
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	int cnt = 0;

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); 
	sa.sin_family = AF_INET;   
	sa.sin_addr.s_addr = INADDR_ANY;   

	
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");


	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		if (cnt == 0)
		{
			cnt++; 

			thread t1(&Server::msgs, this); 
			t1.detach();
			thread t2(&Server::t_msg, this); 
			t2.detach();
		}
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	thread t(&Server::clientHandler, this,client_socket); 
	t.detach();

}


void Server::clientHandler(SOCKET clientSocket)
{
	Helper h;
	int cnt = 0;
	int c = 0, un_S = 0, nxt_unS = 0, fCont_s = 0, pos = 0;
	string un = "", nxun = "", f_cont = "";
	bool b = false;


	while (!b) 
	{
		try
		{
			c = h.getMessageTypeCode(clientSocket);

			if (c == 208) {
				b = true;
				this->_waitingMsg.push(message(c, clientSocket));
			}else if (c == 200) {
				un_S = h.getIntPartFromSocket(clientSocket, 2);
				un = h.getStringPartFromSocket(clientSocket, un_S);

				p_msgF(message(c, un_S, un, clientSocket));
			}
			else{

				fCont_s = h.getIntPartFromSocket(clientSocket, 5);
				f_cont = h.getStringPartFromSocket(clientSocket, fCont_s);

				this->_waitingMsg.push(message(c, f_cont, fCont_s, clientSocket)); 
			}
		}
		catch (const std::exception& e)
		{
			closesocket(clientSocket);
		}
	}
}

void Server::p_clientF(Client client)
{
	queue<Client> temp;

	while (!this->_clients.empty())
	{
		temp.push(this->_clients.front());
		this->_clients.pop();
	}

	this->_clients.push(client);

	while (!temp.empty())
	{
		this->_clients.push(temp.front());
		temp.pop();
	}
}

void Server::t_msg()
{
	while (true)
	{
		unique_lock<mutex> Lock(_lock1);

		while (!this->_waitingMsg.empty()) 
		{
			this->_msgs.push(this->_waitingMsg.front());
			this->_waitingMsg.pop();
		}

		Lock.unlock(); 
		_msgCon.notify_one(); 
	}
}


void Server::p_msgF(message msg)
{
	queue<message> temp;

	while (!this->_waitingMsg.empty()) 
	{
		temp.push(this->_waitingMsg.front());
		this->_waitingMsg.pop();
	}

	this->_waitingMsg.push(msg); 

	while (!temp.empty()) 
	{
		this->_waitingMsg.push(temp.front());
		temp.pop();
	}
}
void Server::d_client(SOCKET& clientSocket)
{
	queue<Client> t;

	while (!this->_clients.empty())
	{
		if (this->_clients.front()._clientSocket != clientSocket) t.push(this->_clients.front());
		this->_clients.pop();
	}

	while (!t.empty())
	{
		this->_clients.push(t.front());
		t.pop();
	}
}




string Server::read_file()
{
	string l = "", info = "";

	ifstream file("data.txt", ios::app); 

	if (file.is_open()) 
	{
		while (getline(file, l)) info += l; 

		file.close(); 
	} 
	else cout << "cant to open file";


	return info;
}

void Server::u_file(string data)
{
	ofstream file("dat.txt");

	if (file.is_open()) 
	{
		file << data; 
		file.close();
	}
	else cout << "cant to open file";

}

void Server::sendAll(string f_cont)
{
	Client c1, c2, temp;
	Helper help;
	int s = this->_clients.size();

	queue<Client> tempQ;

	if (s == 1) 
	{
		help.sendUpMessageToClient(this->_clients.front()._clientSocket, f_cont, this->_clients.front()._cName, "", 1);
	}
	else 
	{
		c1 = this->_clients.front();
		this->_clients.pop();
		c2 = this->_clients.front();

		p_clientF(c1);

		for (int i = 1; i <= s ; i++) 
		{
			tempQ.push(this->_clients.front());
			temp = this->_clients.front();

			help.sendUpMessageToClient(temp._clientSocket, f_cont, c1._cName, c2._cName, i);

			this->_clients.pop();
		}

		while (!tempQ.empty()) 
		{
			this->_clients.push(tempQ.front());
			tempQ.pop();
		}
		
	}
}

