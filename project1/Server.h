#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream> 
#include <queue> 
#include <stack>

using namespace std;

#include "Client.h"

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

	typedef struct message{
		int _code;
		int _unS;
		std::string _un;;
		int _fContS;
		std::string _fCont;
		SOCKET _clientSocket;

		message(int code, SOCKET& clientSocket)
		{

			this->_fCont = "NONE";
			this->_un = "NONE";
			this->_clientSocket = clientSocket;
			this->_code = code;
			this->_unS = -1;
			this->_fContS = -1;
		}

		message(int code, int userNameSize, std::string userName, SOCKET& clientSocket)
		{

			this->_fCont = "NONE";
			this->_un = userName;
			this->_clientSocket = clientSocket;
			this->_code = code;
			this->_unS = userNameSize;
			this->_fContS = -1;
		}

		message(int code, std::string fileContent, int fileContentSize, SOCKET& clientSocket)
		{

			this->_fCont = fileContent;
			this->_un = "NONE";
			this->_clientSocket = clientSocket;
			this->_code = code;
			this->_unS = -1;
			this->_fContS = fileContentSize;
		}
	}message;


private:
	
	void clientHandler(SOCKET clientSocket);
	void d_client(SOCKET& c_socket);
	void p_clientF(Client client);
	void sendAll(string f_dat);
	void p_msgF(message msg);
	void u_file(string info);
	string read_file();
	void accept();
	void t_msg();
	void msgs();
	
	
	queue<message> _waitingMsg;
	queue<Client> _clients;
	SOCKET _serverSocket;
	queue<message> _msgs;
	
};
