#pragma once
#include <WinSock2.h>
#include <iostream>

class Client
{

public:
	Client();
	Client(std::string clientName, SOCKET& socket);
	~Client();
	void startConversation();
	void connect(std::string serverIP, int port);

	std::string _cName;
	SOCKET _clientSocket;
};